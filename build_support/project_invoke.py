# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
import hashlib
import json
import os
import sys
import time
import xml.etree.cElementTree as et

from . import ProjectMap
from . import Options
from . import RepoSet

class RevisionSpecification:
    def __init__(self, revisions=None, repo_set=None, only_projects=None):
        # key is project, value is revision
        if revisions is not None:
            assert isinstance(revisions, dict)
            self._revisions  = revisions
        else:
            self._revisions = {}
            if repo_set is None:
                repo_set = RepoSet()
            # By default, take all projects
            if only_projects is None:
                only_projects = repo_set.projects()
            for p in only_projects:
                try:
                    repo = repo_set.repo(p)
                    rev = repo.git.rev_parse("HEAD", short=True)
                except:
                    continue
                self._revisions[p] = rev

    @classmethod
    def from_xml_file(cls, filename):
        elem = et.ElementTree(file=filename).getroot()
        if elem.tag != 'RevSpec':
            elem = elem.find('RevSpec')
            assert elem is not None
        inst = cls(revisions=elem.attrib)
        return inst

    @classmethod
    def from_cmd_line_param(cls, params):
        return cls(revisions=dict(p.split('=') for p in params))

    def to_cmd_line_param(self):
        revs = []
        for project, revision in self._revisions.items():
            revs.append(project + "=" + revision)
        revs.sort()
        return " ".join(revs)

    def to_elementtree(self):
        elem = et.Element('RevSpec')
        for n, h in sorted(self._revisions.items(), key=lambda x: x[0]):
            elem.set(n, h)
        return et.ElementTree(elem)

    def __str__(self):
        return et.tostring(self.to_elementtree().getroot(), encoding="UTF-8").decode()

    def checkout(self):
        repo_set = RepoSet()
        for (project, revision) in self._revisions.items():
            project_repo = repo_set.repo(project)
            project_repo.git.checkout(["-f", revision])

    def revision(self, project):
        return self._revisions[project]

    def set_revision(self, project, rev):
        assert(project in self._revisions)
        self._revisions[project] = rev


class ProjectInvoke:
    """this object summarizes the component and all options required to
    invoke a build on a single project.  Invocation can take place
    locally or on CI.  ProjectInvoke supports writing status files for
    the invoked build to a network folder, to prevent duplicate builds.

    """

    def __init__(self, options=None, revision_spec=None,
                 project=None, from_string=None, repo_set=None):
        if from_string:
            self.from_string(from_string)
            return

        if not options:
            options = Options()
        self.options = options

        if not project:
            project = ProjectMap().current_project()
        self.project = project

        if not revision_spec:
            revision_spec = RevisionSpecification(repo_set=repo_set)
        self.revision_spec = revision_spec

    def __str__(self):
        tag = et.Element("ProjectInvoke")
        tag.set("Project", self.project)
        tag.append(et.fromstring(str(self.revision_spec)))
        tag.append(self.options.to_elementtree())
        return et.tostring(tag, encoding="UTF-8").decode()

    def from_string(self, string):
        tag = et.fromstring(string)
        self.project = tag.attrib["Project"]
        self.options = Options(from_xml=tag.find("Options"))
        revtag = tag.find("RevSpec")
        self.revision_spec = RevisionSpecification(revisions=revtag.attrib)

    def info_file(self):
        o = self.options
        shard_str = ""
        if o.shard != 0:
            shard_str = "_" + o.shard
        return "/".join([o.result_path,
                         self.project,
                         o.arch,
                         o.config,
                         o.hardware,
                         "_build_info" + shard_str + ".txt"])

    def _read_info(self):
        """returns a dictionary of status content"""
        info_file = self.info_file()
        if not os.path.exists(info_file):
            # sometimes network/mount hiccups make it seem like the
            # file is not there
            time.sleep(0.2)
            if not os.path.exists(info_file):
                return {}
            print("WARN: network hiccup detected")

        attempt_number = 0
        while attempt_number < 5:
            attempt_number += 1
            try:
                info_text = open(info_file, "r").read()
                info_dict = json.loads(info_text)
                return info_dict
            except:
                # network hiccup
                time.sleep(5)

        # failed to parse several times.
        return {}

    def _write_info(self, info_dict):
        info_file = self.info_file()
        info_dir = os.path.dirname(info_file)
        tries = 0
        while not os.path.exists(info_dir) and tries < 20:
            tries += 1
            if tries > 1:
                print("WARN: failed to make info directory: " + info_dir)
                sys.stdout.flush()
                time.sleep(10)
                savedir = os.getcwd()
                try:
                    mount_dir = "/".join(info_dir.split("/")[:5])
                    print("WARN: changing to directory: " + mount_dir)
                    sys.stdout.flush()
                    os.chdir(mount_dir)
                    print("WARN: success")
                    sys.stdout.flush()
                except:
                    pass
                os.chdir(savedir)
            try:
                os.makedirs(info_dir)
            except:
                # race condition means some other build may have
                # created the directory.
                pass
        open(info_file, "w").write(json.dumps(info_dict))

    def get_info(self, key, block=True):
        for _ in range(0, 10):
            info = self._read_info()
            if key in info:
                return info[key]
            if not block:
                return None
            # possible that the data has not been flushed to the
            # server
            time.sleep(1)

    def set_info(self, key, value):
        info_dict = self._read_info()
        info_dict[key] = value
        self._write_info(info_dict)

    def hash(self, salt):
        """provides a string value to uniquely identify a build.  This is used
        to find builds and resolve clashes between similar builds on
        the jenkins server"""
        return hashlib.md5((salt + str(self)).encode("utf-8")).hexdigest()

    def to_short_string(self):
        items = [self.project,
                 self.options.arch,
                 self.options.config,
                 self.options.hardware]
        if self.options.shard != "0":
            items.append(self.options.shard)
        return " ".join(items)
