# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import glob
import os
import subprocess
from . import cpu_count
from . import Options
from . import ProjectMap
from . import run_batch_command
from . import rmtree
from . import Export


class AndroidBuilder(object):
    def __init__(self, src_location, module):
        self._options = Options()
        self._project_map = ProjectMap()
        self._src_dir = os.path.join(self._project_map._source_root, "repos",
                                     "android")
        self._android_orig_src = src_location
        self._android_target = "cel_apl-eng"
        self._env = {
           'ANDROID_TARGET': self._android_target,
           'ANDROID_SOURCE': self._src_dir,
           'ANDROID_MODULE': module,
           'NUM_CPUS': str(cpu_count()),
        }
        self._build_helper = os.path.join(self._project_map.source_root(),
                                          "repos/mesa_ci/build_support",
                                          "android_builder.sh")

        # Location of mesa source to build
        self._mesa_src = os.path.join(self._project_map._source_root, "repos",
                                      "mesa")
        if not os.path.exists(self._mesa_src):
            assert os.path.exists(self._mesa_src), ("ERROR: Mesa source directory "
                                              "not found: {}".format(self._mesa_src))


    def build(self):
        # Copy android source tree to repos
        if not os.path.exists(self._src_dir):
            print("Copying android source from: {}".format(self._android_orig_src))
            run_batch_command(["cp", "-al", os.path.expanduser(self._android_orig_src),
                               self._src_dir])
        # Local location of mesa source in Android tree
        self._mesa_local_src = os.path.join(self._project_map._source_root,
                                            "repos/android/vendor/intel",
                                            "external/project-celadon/mesa")
        # Copy mesa source to subdir in android tree
        if os.path.exists(self._mesa_local_src):
            if os.path.islink(self._mesa_local_src):
                os.remove(self._mesa_local_src)
            else:
                rmtree(self._mesa_local_src)
        run_batch_command(["cp", "-al", self._mesa_src, self._mesa_local_src])
        # Remove patch(es) from the celadon project that do not apply to
        # mesa master, since 'lunch' treats patch conflicts as fatal
        bad_patches = ['0001-use-the-private-drm-lib-name.patch']
        for patch in bad_patches:
            patch_path = os.path.join(self._src_dir,
                                      'vendor/intel/utils/android_p/google_diff/'
                                      + self._android_target.split('-')[0],
                                      'vendor/intel/external/project-celadon/mesa',
                                      patch)
            if os.path.exists(patch_path):
                print("Removing bad patch from celadon: {}".format(patch_path))
                os.remove(patch_path)
        # apply patches if they exist
        for patch in sorted(glob.glob(os.path.join(
                            self._project_map.project_build_dir(),
                            "*.patch"))):
            os.chdir(self._mesa_local_src)
            try:
                run_batch_command(["git", "am", patch])
            except subprocess.CalledProcessError:
                print("WARN: failed to apply patch: {}".format(patch))
                run_batch_command(["git", "am", "--abort"])
        try:
            run_batch_command([self._build_helper, 'build'], env=self._env)
        except subprocess.CalledProcessError:
            Export().create_failing_test("Android Build Test",
                                         ("ERROR: Failed to build Mesa. "
                                          "See the console log for the "
                                          "android-buildtest component "
                                          "for more details."))

    def clean(self):
        if os.path.exists(self._src_dir):
            rmtree(self._src_dir)

    def test(self):
        pass
