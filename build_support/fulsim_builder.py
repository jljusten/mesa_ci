# Copyright (C) Intel Corp.  2019.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import os
import subprocess
import urllib
import zipfile
from . import Export
from . import Options
from . import ProjectMap
from . import rmtree
from . import run_batch_command

# Note: values are urls templates, {0} will be replaced with the fulsim build
# number
fulsim_urls = {
    'tgl': ['https://gfx-assets.intel.com/artifactory/gfx-sim-assets-fm/fulsim/tgllp/Linux/{0}/Fulsim_TGLLP-{0}.zip'],
    'ats': ['https://gfx-assets.intel.com/artifactory/gfx-sim-assets-fm/cobalt/gen12hp/Linux/{0}/Cobalt_Gen12HP-{0}.zip',
            'https://gfx-assets.igk.intel.com/artifactory/gfx-cobalt-snapshot-assets-fm/Cobalt/Linux/ATS/{0}/ATS-{0}-Linux.zip '],
}


class FulsimBuilder(object):
    def __init__(self, buildnum):
        self._options = Options()
        self._project_map = ProjectMap()
        self._env = {}
        self.hardware = self._options.hardware
        self.buildnum = buildnum
        # Install path is /tmp/build_root/opt/fulsim/<hardware>
        self.fulsim_install_path = (self._project_map.build_root()
                                    + '/opt/fulsim/' + self.hardware)
        if self.hardware not in fulsim_urls:
            assert False, ("ERROR: No fulsim source url defined for hardware"
                           ": {}".format(self.hardware))

    def build(self):
        fulsim_archive_path = os.path.join("/tmp", "fulsim_" + self.hardware
                                           + "_" + self.buildnum + ".zip")
        for url in fulsim_urls[self.hardware]:
            try:
                print("Attempting to download fulsim from: "
                      "{}".format(url.format(self.buildnum)))
                urllib.request.urlretrieve(url.format(self.buildnum),
                                           fulsim_archive_path)
                print("Found archive at the given url.")
            except urllib.error.HTTPError:
                print("The requested fulsim version was not found at the given "
                      "url.")
        if not os.path.exists(fulsim_archive_path):
            assert False ("ERROR: Unable to retrieve the requested fulsim "
                          "version")

        if not os.path.exists(self.fulsim_install_path):
            os.makedirs(self.fulsim_install_path)

        print("Extracting fulsim archive {}".format(fulsim_archive_path))
        try:
            run_batch_command(["unzip", "-q", fulsim_archive_path, "-d",
                               self.fulsim_install_path])
        except subprocess.CalledProcessError:
            if os.path.exists(self.fulsim_install_path):
                rmtree(self.fulsim_install_path)
            assert False, ("ERROR: The fulsim archive is invalid and as been "
                           "removed. This could be caused by a corrupted "
                           "download or if the requested fulsim version is "
                           "no longer available.")
        finally:
            os.remove(fulsim_archive_path)

        Export().export()

    def clean(self):
        if os.path.exists(self.fulsim_install_path):
            rmtree(self.fulsim_install_path)

    def test(self):
        pass
