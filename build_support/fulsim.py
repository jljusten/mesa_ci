#!/usr/bin/python2
import os
import sys
from collections import namedtuple
from . import Options
from . import ProjectMap


class Fulsim(object):

    def __init__(self):
        # key_file - a file under the mesa repo for determining whether or not
        # the mesa branch supports this platform
        self.platform_keyfile = {
            'tgl': 'src/intel/genxml/gen12.xml',
            'ats': 'src/intel/genxml/gen12.xml',
        }
        self._hardware = Options().hardware
        self._arch = Options().arch
        self._project_map = ProjectMap()
        self._build_root = self._project_map.build_root()
        self._mesa_repo_dir = self._project_map.source_root() + "/repos/mesa/"

    def is_supported(self):
        """ Determines if the hardware is supported for running on fulsim """
        if self._hardware not in self.platform_keyfile:
            print("Environment check failure: Unable to find the expected "
                  "fulsim version for this platform.")
            return False
        # Check for existence of the keyfile if there is one
        if self.platform_keyfile[self._hardware]:
            if not os.path.exists(self._mesa_repo_dir + "/"
                                  + self.platform_keyfile[self._hardware]):
                print("Environment check failure: An unsupported Mesa version "
                      "was detected. Make sure the correct Mesa sha was "
                      "specified.")
                return False
        # 32-bit only
        if self._arch == "m32":
            print("Environment check failure: 32-bit is not supported.")
            return False
        return True

    def get_env(self):
        env = {}
        # Additional configuration if running in simulation
        if self._hardware in self.platform_keyfile:
            sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                                         "..", "repos", "sim-drm"))
            import sim_drm
            aubload = os.path.expanduser(self._build_root
                                         + '/opt/fulsim/' + self._hardware
                                         + '/AubLoad')
            env = sim_drm.get_env(self._hardware, aubload=aubload,
                                  libsim_drm=(self._build_root
                                              + '/lib/libsim-drm.so'))
        return env
