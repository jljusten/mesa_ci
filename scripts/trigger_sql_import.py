import sys
import os
try:
    from urllib2 import urlopen, quote
except:
    from urllib.request import urlopen, quote

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), ".."))
import build_support as bs

import_url = ("http://otc-mesa-ci.jf.intel.com/job/ImportResults/buildWithParameters?"
              "token=noauth&"
              "url={0}".format(quote(os.environ["BUILD_URL"])))

print("triggering: " + import_url)
urlopen(import_url)

